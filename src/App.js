import React from 'react';
import SearchAppBar from "./components/Header";
import "./styles.css";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Main from "./pages/main";
import BottomAppBar from "./pages/barbottom";

const theme = createMuiTheme({
	typography: {
		useNextVariants: true,
	},
	palette: {
		type: 'dark',
		primary: {
			light: "#00998d",
			main: "#009688",
			dark: "#33ab9f",
		},
		secondary: {
			light: "#00a152",
			main: "#00e676",
			dark: "#33eb91",
		},
	},
});

const App = () => (
	<div className="App">
		<MuiThemeProvider theme={theme}>
			<div>
				<CssBaseline />
				<Main />
				<BottomAppBar />
			</div>
		</MuiThemeProvider>
	</div>
);



export default App;
