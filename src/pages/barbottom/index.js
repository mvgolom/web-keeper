import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Avatar from '@material-ui/core/Avatar';
import MenuIcon from '@material-ui/icons/Menu';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import MoreIcon from '@material-ui/icons/MoreVert';
import api from "../../services/api"
import Modal from '@material-ui/core/Modal';
const styles = theme => ({
	text: {
		paddingTop: theme.spacing.unit * 2,
		paddingLeft: theme.spacing.unit * 2,
		paddingRight: theme.spacing.unit * 2,
	},
	paper: {
		paddingBottom: 50,
	},
	list: {
		//maxWidth: "700px",
		marginBottom: theme.spacing.unit * 2,
	},
	subHeader: {
		backgroundColor: theme.palette.background.paper,
	},
	appBar: {
		top: 'auto',
		bottom: 0,
	},
	toolbar: {
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	fabButton: {
		position: 'absolute',
		zIndex: 1,
		top: -30,
		left: 0,
		right: 0,
		margin: '0 auto',
	},
	paper2:{
		position: 'absolute',
		width: theme.spacing.unit * 50,
		backgroundColor: theme.palette.background.paper,
		boxShadow: theme.shadows[5],
		padding: theme.spacing.unit * 4,
		outline: 'none',
	},
});
function rand() {
	return Math.round(Math.random() * 20) - 10;
}
function getModalStyle() {
	const top = 50 + rand();
	const left = 50 + rand();
  
	return {
	  top: `${top}%`,
	  left: `${left}%`,
	  transform: `translate(-${top}%, -${left}%)`,
	};
  }

class BottomAppBar extends Component {
	state = {
		messages: [],
		open: false,
	}
	componentDidMount(){
		this.loadMessages();
	}

	loadMessages = async () => {
		const response = await api.get('/msgs');
		this.setState({ messages: response.data })
		console.log(response.data);
	}
	print_hello(){
		console.log("hello hello hello hello hello")
	}
	handleOpen = () => {
		this.setState({ open: true });
	};
	
	handleClose = () => {
		this.setState({ open: false });
	};
	

	render(){
		const { classes } = this.props;
		return (
			<React.Fragment>
				<CssBaseline />
				<Paper square className={classes.paper}>
					<List className={classes.list}>
						{this.state.messages.map(msg => (
							<Fragment key={msg.id}>
								<ListItem button>
									<ListItemText primary={msg.msg} secondary={msg.date} />
								</ListItem>
							</Fragment>
						))}
					</List>
				</Paper>
				<AppBar position="fixed" color="primary" className={classes.appBar}>
					<Toolbar className={classes.toolbar}>
						<IconButton color="inherit" aria-label="Open drawer">
							<MenuIcon />
						</IconButton>
						<Fab color="secondary" aria-label="Add" className={classes.fabButton} onClick={this.handleOpen}>
							<AddIcon />
						</Fab>
						<Modal
						aria-labelledby="simple-modal-title"
						aria-describedby="simple-modal-description"
						open={this.state.open}
						onClose={this.handleClose}>
							<div style={getModalStyle()} className={classes.paper2}>
								<Typography variant="h6" id="modal-title">
								Text in a modal
								</Typography>
								<Typography variant="subtitle1" id="simple-modal-description">
								Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
								</Typography>
							</div>	
						</Modal>
						<div>
							<IconButton color="inherit">
								<SearchIcon />
							</IconButton>
							<IconButton color="inherit">
								<MoreIcon />
							</IconButton>
						</div>
					</Toolbar>
				</AppBar>
			</React.Fragment>
		);
	}
}

BottomAppBar.propTypes = {
	classes: PropTypes.object.isRequired,
};



export default withStyles(styles)(BottomAppBar);