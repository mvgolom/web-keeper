import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles';
//import api from "../../services/api"
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { unstable_Box as Box } from '@material-ui/core/Box';
//import "./styles.css"

const styles = theme => ({
    root: {
        padding: "10px",
        marginBottom:"10px"
    },
    messages:{
        maxWidth: "700px",
        margin: "20px  auto 0"
    },
    date:{
        color:"#66d9ef"
    },
    text:{
        fontSize:16,
        fontweight:"bolder",

    }
});

class Main extends Component {
    state = {
        msgs: [
            {
            "date": "Fri, 03 May 2019 19:04:20 GMT",
            "id": 1,
            "msg": "mudei essa segunda vez"
          },
          {
            "date": "Fri, 03 May 2019 16:04:20 GMT",
            "id": 1061928347,
            "msg": "teste da primeira mensagem"
          },
          {
            "date": "Sat, 04 May 2019 02:01:37 GMT",
            "id": 3930550189,
            "msg": "teste da primeira mensagem"
          },
          {
            "date": "Sat, 04 May 2019 02:01:38 GMT",
            "id": 478672919,
            "msg": "teste da primeira mensagem"
          },
          {
            "date": "Sat, 04 May 2019 02:01:39 GMT",
            "id": 2430336202,
            "msg": "teste da primeira mensagem"
          },
          {
            "date": "Sat, 04 May 2019 02:01:40 GMT",
            "id": 86820771,
            "msg": "teste da primeira mensagem"
          }
        ],
    }
    componentDidMount() {
        console.log(this.state.msgs);
        // this.loadMessages();
    }

    loadMessages = async () => {
        // const response = await api.get('/msgs');
        // this.setState({ msgs: response.data })
        // this.setState({ msgs: response })
        // console.log(this.state.msgs);
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.messages} >
                {this.state.msgs.map(msg => (
                    <Paper className={classes.root} elevation={1} key={msg.id}>
                        <Typography component="div">
                            <Box className={classes.text} textAlign="justify" m={1}>
                                {msg.msg}
                            </Box>
                            <Box className={classes.date} textAlign="right" m={1}>
                                {msg.date}
                            </Box>
                        </Typography>
                        
                    </Paper>
                ))}
            </div>
        )
    }
}

export default withStyles(styles)(Main);