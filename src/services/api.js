import axios from 'axios';

const api = axios.create({ 
    baseURL:'http://127.0.0.1:5000/',
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/vnd.github.v3+json'
    }
});

export default api;